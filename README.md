### Angular, prettier, webstorm

https://medium.com/@victormejia/setting-up-prettier-in-an-angular-cli-project-2f50c3b9a537
https://medium.com/@ofirrifo/setup-prettier-with-angular-cli-webstorm-d339097595cf
https://github.com/prettier/tslint-config-prettier
https://www.daptontechnologies.com/angular-prettier-husky/

npm i prettier
npm i -D tslint-config-prettier

Add it in preferences:

```javascript
  "extends": ["tslint:latest", "tslint-config-prettier"]
```

Or better this

```javascript
  "extends": ["tslint:recommended", "tslint-config-prettier"],
```

Use the “Reformat with Prettier” action (Alt-Shift-Cmd-P on macOS or Alt-Shift-Ctrl-P on Windows and Linux) or find it using the “Find Action” popup (Cmd/Ctrl-Shift-A)

### Run on save

> not enable on this project since we use pretty quick

To run Prettier on save, tick the "Run on save for files" option in Preferences/Settings | Languages \& Frameworks | JavaScript | Prettier.

---

#### Custom prettier configuration

On the root of the project, create a file called .prettierrc with these prettier options,

```javascript
{
    "bracketSpacing": true,
    "semi": true,
    "singleQuote": true,
    "trailingComma": "es5",
    "printWidth": 140
}
```

Create another file .prettierignore at the root of the project

```
package.json
package-lock.json
yarn.lock
dist
```

---

### HUSKY

https://www.npmjs.com/package/husky
https://codeburst.io/continuous-integration-lint-staged-husky-pre-commit-hook-test-setup-47f8172924fc

BEST ARTICLE PRETTIER & HUSKY -> https://www.daptontechnologies.com/angular-prettier-husky/

We want prettier to only run on our changed files, instead of running on whole project. pretty-quick does exactly that.

https://www.npmjs.com/package/pretty-quick

```
npm install --save-dev pretty-quick
```

Add in `package.json`

```javascript
"husky": {
    "hooks": {
        "pre-commit": "pretty-quick --staged && ng lint && ng test",
        "pre-push": "ng build --aot true"
    }
}
```

To avoid run tests and build it can be simplify:

```
"husky": {
   "hooks": {
    "pre-commit": "pretty-quick --staged && ng lint"
   }
}
```
````````
