import { NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: 'login',
        loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
      },
      { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule  {
  constructor(private router: Router) {
    console.log(router.config)
    // Add dynamic routes - version 1
    /*
    router.config.push(
      {
        path: 'catalog',
        component: CatalogComponent,
      },
    );
    router.config.push(
      { path: '**', redirectTo: 'login'}
    );
    */

    // Add dynamic routes - version 2
    router.resetConfig([
      ...router.config,
      {
        path: 'catalog',
        // component: CatalogComponent,
        loadChildren: () => import(
          './features/catalog/catalog.module'
        ).then(m => m.CatalogModule),

        canActivate: [AuthGuard]
      },
      { path: '**', redirectTo: 'login'}
    ]);

  }
}
