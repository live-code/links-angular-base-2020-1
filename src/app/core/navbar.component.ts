import { Component, OnInit } from '@angular/core';
import { ThemeService } from './theme.service';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  template: `

    <div 
      [ngClass]="{
        'bg-dark': themeService.value === 'dark',
        'bg-light': themeService.value === 'light'
      }"
      
    >
      LOGO
      <button  [routerLink]="'/login'">login</button>
      <button appIfLogged  routerLink="/catalog">catalog</button>
      <button  routerLink="/contacts">contacts</button>
      <button (click)="logoutHandler()">lougout</button>
     <!-- <button
        appPad="10" 
        appUrl="http://www.fabiobiondi.io"
        (loaded)="doSomething()"
      >goto website</button>-->
    </div>
  `,
  styles: [
  ]
})
export class NavbarComponent {

  constructor(
    public authService: AuthService,
    public themeService: ThemeService,
    private router: Router
  ) {
    // this.themeService.value = 'light'
  }

  doSomething() {
    alert('loaded')
  }

  logoutHandler() {
    this.authService.logout();
    this.router.navigateByUrl('login');
  }

}
