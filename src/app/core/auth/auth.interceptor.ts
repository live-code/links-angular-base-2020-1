import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let clonedReq = req;
    if (this.authService.isLogged()) {
      clonedReq = req.clone({
        setHeaders: { Authorization: this.authService.token}
      });
    }
    return next.handle(clonedReq)
      .pipe(
        catchError(err => {
          console.log('errore',err);
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
              case 404:
                alert('errore server');
                // this.router.navigateByUrl('login')
                break;

              default:
                break;
            }
          }
          // return of(null);
          return throwError(err);
        })
      )
  }

}

