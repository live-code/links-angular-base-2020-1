import { Directive, HostBinding } from '@angular/core';
import { AuthService } from './auth.service';

@Directive({
  selector: '[appIfLogged]'
})
export class IfloggedDirective {
  @HostBinding('style.display') get display() {
    return this.authService.isLogged() ? null : 'none';
  }

  constructor(private authService: AuthService) {

  }

}
