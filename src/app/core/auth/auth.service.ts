import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from './auth';
import { Observable, Subscription } from 'rxjs';
import { delay, map, share, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data: Auth;

  constructor(private http: HttpClient) {}

  login(user: string, pass: string): Observable<Auth> {

    const params = new HttpParams()
      .set('username', user)
      .set('password', pass)

    const obs = this.http.get<Auth>(`http://localhost:3000/login`, { params })
      .pipe(
        share(),
        delay(2000)
      );

    obs.subscribe(res => {
      this.data = res;
      localStorage.setItem('token', res.token)
    });

    return obs;

  }

  isLogged() {
    return !!localStorage.getItem('token');
    // return !!this.data;
  }

  get token() {
    return this.data ? this.data.token : '';
  }

  logout() {
    this.data = null;
    localStorage.removeItem('token');
  }
}
