import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { RouterModule } from '@angular/router';
import { IfloggedDirective } from './auth/iflogged.directive';

@NgModule({
  declarations: [
    NavbarComponent,
    IfloggedDirective,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    NavbarComponent
  ],
})
export class CoreModule { }
