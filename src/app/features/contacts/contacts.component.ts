import { Component, OnDestroy, OnInit, Renderer2, RendererFactory2 } from '@angular/core';
import { LangSubscribe } from '../../shared/decorators/lang-subscribe.decorator';


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
@LangSubscribe()
export class ContactsComponent {
  myConfig = [99, 19, 3, 5, 2, 3]

  constructor() {
    setTimeout(() => {
      this.myConfig = [5, 19, 3, 5, 2, 3]
    }, 2000)
  }
}
