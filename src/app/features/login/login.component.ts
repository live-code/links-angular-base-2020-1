import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme.service';
import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  template: `

    <div [ngClass]=""></div>
    <app-panel 
      title="Login" 
      icon="fa fa-facebook"
      (iconClick)="openUrl('http://www.facebook.com/')"
    >
      <form #f="ngForm" (submit)="loginHandler(f.value)">
        <input type="text" [ngModel] name="user">
        <input type="text" [ngModel] name="pass">
        <button
          type="submit">Login</button>
      </form>
    </app-panel>

    <app-panel 
      title="Hai perso la pass" 
      icon="fa fa-map"
      (iconClick)="doSomething()"
    >
      Lorem ipsum - Map
    </app-panel>

    {{themeService.value}}
    <hr>
    
    token: {{authService.token}}
    <br>
    
  `,
})
export class LoginComponent {

  constructor(
    public themeService: ThemeService,
    public authService: AuthService,
    private router: Router
  ) {
    console.log(themeService)

    setTimeout(() => {
      themeService.value = 'dark'
    }, 3000)
  }

  openUrl(url: string) {
    window.open(url)
  }

  doSomething() {
    console.log('console.log')
  }

  loginHandler(formData: any) {
    console.log(formData)
    this.authService.login(formData.user, formData.pass)
      .subscribe(res => {
        this.router.navigateByUrl('catalog')
      });
  }
}
