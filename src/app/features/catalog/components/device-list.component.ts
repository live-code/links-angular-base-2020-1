import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Device } from '../../../model/device';

@Component({
  selector: 'app-device-list',
  template: `
    
    <input type="text" [(ngModel)]="textSearch">
   
    <div
      *ngFor="let device of items | search: textSearch"
      class="list-group-item"
      [ngClass]="{'active': device.id === selected?.id}"
      (click)="setActive.emit(device)"
    >
      <app-os-icon [os]="device.os"></app-os-icon>
      <span>{{device.label}} - {{device.memory | memory: 'gb':'aaa'}}</span>
      <i class="fa fa-star" *ngFor="let star of device.rate | intToArray"></i>

      <div class="pull-right">
        <app-price [price]="device.price"></app-price>
        
        <i class="fa fa-info-circle" [routerLink]="'/catalog/details/' + device.id"></i>
        <i 
          class="fa fa-trash" 
          (click)="deleteHandler(device, $event)"
        ></i>
      </div>
    </div>
  `
})
export class DeviceListComponent {
  textSearch: string;
  @Input() items: Device[];
  @Input() selected: Device;
  @Output() setActive: EventEmitter<Device> = new EventEmitter();
  @Output() delete: EventEmitter<Device> = new EventEmitter();

  deleteHandler(device: Device, event: MouseEvent) {
    event.stopPropagation();
    this.delete.emit(device);
  }
}
