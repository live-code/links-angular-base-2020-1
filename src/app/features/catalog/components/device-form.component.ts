import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Device } from '../../../model/device';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-device-form',
  template: `
    <!--Form-->
    <form #f="ngForm" (submit)="save.emit(f.value)">
      <input
        #labelRef
        #labelInput="ngModel"
        [ngModel]="selected.label"
        name="label"
        type="text"
        class="form-control"
        [ngClass]="{'is-invalid': labelInput.invalid && f.dirty, 'is-valid': labelInput.valid }"
        required minlength="3"
      >

      <div *ngIf="labelInput.errors?.required && f.dirty">Field required</div>
      <div *ngIf="labelInput.errors?.minlength && f.dirty">Min 3 chars</div>

      <hr>
      <input
        #priceInput="ngModel"
        [ngModel]="selected.price"
        name="price"
        type="number" class="form-control"
        [ngClass]="{'is-invalid': priceInput.invalid && f.dirty, 'is-valid': priceInput.valid }"
        required
      >
      <button type="submit" [disabled]="f.invalid">
        {{selected.id ? 'EDIT' : 'ADD'}}
      </button>
      <button type="button" (click)="clearHandler()">Clear</button>
    </form>
  `,

})
export class DeviceFormComponent implements OnChanges, AfterViewInit {
  @ViewChild('f', { static: true }) form: NgForm;
  @ViewChild('labelRef') labelInput: ElementRef<HTMLInputElement>;
  @Input() selected: Device;
  @Input() color: string;
  @Output() clear: EventEmitter<void> = new EventEmitter<void>();
  @Output() save: EventEmitter<Device> = new EventEmitter<Device>();

  ngAfterViewInit() {
    this.labelInput.nativeElement.focus();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.selected && !this.selected?.id) {
      this.form.reset();
    }
  }

  clearHandler() {
    this.clear.emit();
  }
}
