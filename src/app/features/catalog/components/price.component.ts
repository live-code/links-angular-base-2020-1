import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-price',
  template: `
    <span
      [style.color]="price > 1000 ? 'orange' : null "
    >€ {{price | number: '1.2-2'}}</span>
  `,
})
export class PriceComponent {
  @Input() price: number;
}
