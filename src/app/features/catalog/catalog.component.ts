import { Component } from '@angular/core';
import { CatalogService } from './services/catalog.service';

@Component({
  selector: 'app-catalog',
  template: `
    <div class="container mt-3">
      
      <div 
        class="alert alert-danger"
        *ngIf="catalogService.error"
      > Errore!</div>
      
      <app-device-form
        color="red"
        [selected]="catalogService.active"
        (clear)="catalogService.clear()"
        (save)="catalogService.save($event)"
      ></app-device-form>

      <hr>
      
      <app-device-list
        [items]="catalogService.devices"
        [selected]="catalogService.active"
        (setActive)="catalogService.setActive($event)"
        (delete)="catalogService.deleteHandler($event)"
      ></app-device-list>
    </div>
  `,
})
export class CatalogComponent {

  constructor(public catalogService: CatalogService) {
    catalogService.getAll();
  }

}
