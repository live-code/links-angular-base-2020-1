import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogComponent } from './catalog.component';
import { CatalogDetailsComponent } from './catalog-details.component';
import { DeviceFormComponent } from './components/device-form.component';
import { DeviceListComponent } from './components/device-list.component';
import { OsIconComponent } from './components/os-icon.component';
import { PriceComponent } from './components/price.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CatalogHelpComponent } from './catalog-help.component';

@NgModule({
  declarations: [
    // feature: Catalog
    CatalogComponent,
    CatalogDetailsComponent,
    DeviceFormComponent,
    DeviceListComponent,
    OsIconComponent,
    PriceComponent,
    CatalogHelpComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent },
      { path: 'details/:id', component: CatalogDetailsComponent },
      { path: 'help', component: CatalogHelpComponent },
    ])
  ]
})
export class CatalogModule { }
