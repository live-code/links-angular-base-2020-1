import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Device } from '../../model/device';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-catalog-details',
  template: `
    <div class="alert alert-danger" *ngIf="error">
      Not found
    </div>
    <button routerLink="/catalog">Back</button>
    <h1>{{data?.label}}</h1>
    <div>{{data?.price}}</div>
  `,
  styles: [
  ]
})
export class CatalogDetailsComponent implements OnInit {
  data: Device;
  error: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) {
    const id = activatedRoute.snapshot.params.id;
    this.http.get<Device>(`${environment.API}/devices/${id}`)
      .subscribe(
        res => this.data = res,
        err => this.error = err
      );
  }

  ngOnInit(): void {
  }

}
