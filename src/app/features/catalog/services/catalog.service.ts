import { Injectable } from '@angular/core';
import { Device } from '../../../model/device';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  devices: Device[];
  active: Device = {} as Device;
  error;

  constructor(private http: HttpClient){}

  getAll() {
    this.http.get<Device[]>(`${environment.API}/devices`)
      .subscribe(
        (result) => {
          console.log(result)
          this.devices = result;
        },
        err => {
          console.log('ERRORE!')
          this.error = true;
        }
      );
  }

  save(device: Device) {
    if (this.active && this.active.id) {
      this.edit(device);
    } else {
      this.add(device);
    }
  }

  edit(device: Device) {

    this.http.patch<Device>(`${environment.API}/devices/${this.active.id}`, device)
      .subscribe((result) => {
        const index = this.devices.findIndex(d => d.id === this.active.id);
        this.devices[index] = result;
      });
  }


  add(device: Device) {

    this.http.post<Device>(`${environment.API}/devices/`, device)
      .subscribe((res) => {
        this.devices.push(res);
        this.clear();
      });
  }


  deleteHandler(device: Device) {
    this.http.delete(`${environment.API}/devices/${device.id}`)
      .subscribe(() => {
        const index = this.devices.findIndex(d => d.id === device.id);
        this.devices.splice(index, 1);

        if (device.id === this.active?.id) {
          this.clear();
        }
      });
  }

  setActive(device: Device) {
    this.active = device;
  }

  clear() {
    this.active = {} as Device;
  }
}
