export interface Device {
  id?: number;
  label: string;
  os: string;
  rate: number;
  releaseDate?: number;
  madeIn?: string;
  memory?: number;
  price?: number;
}
