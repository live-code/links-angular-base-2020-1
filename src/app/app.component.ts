import { Component } from '@angular/core';
import { ThemeService } from './core/theme.service';

@Component({
  selector: 'app-root',
  template: `
    
    <app-navbar></app-navbar>
    
    <div class="container mt-3">
      <router-outlet></router-outlet>
    </div>
    
  `,

})
export class AppComponent {

}
