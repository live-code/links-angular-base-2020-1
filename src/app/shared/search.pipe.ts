import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
  pure: true
})
export class SearchPipe implements PipeTransform {

  transform(items: any[], text: string): unknown {
    if (!items) return;
    if (!text || text === '') {
      return items;
    }

    return items.filter(item => {
      return item.label.toLowerCase().indexOf(text.toLowerCase()) !== -1
    });
  }

}
