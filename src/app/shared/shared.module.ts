import { NgModule } from '@angular/core';
import { HelloComponent } from './hello.component';
import { PanelComponent } from './panel.component';
import { MemoryPipe } from './memory.pipe';
import { IntToArrayPipe } from './int-to-array.pipe';
import { SearchPipe } from './search.pipe';
import { PadDirective } from './pad.directive';
import { UrlDirective } from './url.directive';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ChartComponent } from './chart.component';

export const COMPONENTS =  [
  // shared
  HelloComponent,
  PanelComponent,
  MemoryPipe,
  IntToArrayPipe,
  SearchPipe,
  PadDirective,
  UrlDirective,
  ChartComponent
];

@NgModule({
  declarations: [
    ...COMPONENTS,

  ],
  exports: [
    ...COMPONENTS,
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule {

}
