import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-panel',
  template: `
  
    <div class="card">
      <div 
        class="card-header"
        (click)="isOpen = !isOpen"
      >
        {{title}}
        
        <div class="pull-right">
          <i (click)="iconClick.emit()" 
            *ngIf="icon" [ngClass]="icon"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
    
  `
})
export class PanelComponent {
  @Input() title = 'widget';
  @Input() icon: string;
  @Output() iconClick: EventEmitter<void> = new EventEmitter();

  isOpen = true;
}



















