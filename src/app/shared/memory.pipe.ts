import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'memory'
})
export class MemoryPipe implements PipeTransform {

  transform(value: number, unit: string, acaso: any): unknown {
    return (value / 1000).toFixed(1) + 'gb'
  }

}
