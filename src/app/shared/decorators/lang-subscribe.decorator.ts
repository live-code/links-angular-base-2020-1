// ------------
import { AppModule } from '../../app.module';

export function LangSubscribe(): any {
  return (constructor: any) => {
    constructor.ɵcmp.onInit = () => {
      console.log('onInit')
    }

    constructor.ɵcmp.onDestroy = () => {
      console.log('onDestroy')
    }

    console.log(constructor.ɵcmp)
/*
    if (constructor.ɵcmp) {
      // Using private API until the corresponding Angular issues are resolved.
      // Issues:  https://github.com/angular/angular/issues/31495, https://github.com/angular/angular/issues/30497
      // PR:      https://github.com/angular/angular/pull/35464
      destroy = <Function>instanceConstructor.ɵcmp.onDestroy ?? noop;
      instanceConstructor.ɵcmp.onDestroy = __takeUntilDestroy__destroy__;
    } else {
      destroy = <Function>instanceConstructor.prototype.ngOnDestroy ?? noop;
      instanceConstructor.prototype.ngOnDestroy = __takeUntilDestroy__destroy__;
    }*/

  }
}
