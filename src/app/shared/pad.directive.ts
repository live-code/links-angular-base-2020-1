import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirective {
  @HostBinding('style.padding') get padding() {
    return `${this.appPad}px`;
  }

  @Input() appPad

  constructor() { }

}
