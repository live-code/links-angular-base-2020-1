import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[appUrl]'
})
export class UrlDirective {
  @Input() appUrl: string;
  @Output() loaded = new EventEmitter();

  @HostListener('click')
  clickHandler() {
    window.open(this.appUrl)

    setTimeout(() => {
      this.loaded.emit()
    }, 1000)


  }

}
